import 'package:flutter/material.dart';

/*
BASE
void main() {
  runApp(MaterialApp(
      title: "Contador de Pessoas", home: Container(color: Colors.white)));
}
*/

/* USO DE IMAGENS
Editar arquivo pudspec.yaml

Exemplo:
  assets:
    - images/restaurantJapanese.jpg

> lembrar de dar um packages get ao final
*/

void main() {
  runApp(MaterialApp(title: "Contador de Pessoas", home: Home()));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _people = 0;
  String _fraseEntrada = "Não pode entrar.";
  String _assetCanEnter = "images/canEnter.jpg";
  String _assetCantEnter = "images/cantEnter.jpg";
  String _wtfImage = "images/gandalf.gif";
  String _enterImage = "images/cantEnter.jpg";

  void _changePeople(int changeDelta) {
    if (changeDelta == 1) {
      setState(() {
        _people++;
        if (_people >= 0) {
          _enterImage = _assetCanEnter;
          _fraseEntrada = "Pode entrar.";
        } else {
          _enterImage = _wtfImage;
          _fraseEntrada = "WTF!?!?";
        }
      });
    } else {
      setState(() {
        _people--;
        if (_people >= 0) {
          _enterImage = _assetCantEnter;
          _fraseEntrada = "Pra fora";
        } else {
          _enterImage = _wtfImage;
          _fraseEntrada = "WTF!?!?";
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          _enterImage,
          fit: BoxFit.cover,
          height: 10000,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Pessoas: $_people",
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.all(10),
                  child: FlatButton(
                    onPressed: () {
                      _changePeople(1);
                    },
                    child: Text(
                      "+1",
                      style: TextStyle(fontSize: 40, color: Colors.white),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: FlatButton(
                    onPressed: () {
                      _changePeople(-1);
                    },
                    child: Text(
                      "-1",
                      style: TextStyle(fontSize: 40, color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
            Text(
              _fraseEntrada,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                  fontSize: 30),
            )
          ],
        )
      ],
    );
  }
}
