# Entry Counter

An extremely basic project in Flutter to understand how widgets work. It serves as an example for overlaying widgets (Stack).

# Sample screens
![enter image description here](https://gitlab.com/ariltonJAguilar/entry-counter/-/raw/master/samples/sample1.jpg)
![enter image description here](https://gitlab.com/ariltonJAguilar/entry-counter/-/raw/master/samples/sample2.jpg)